package com.dxp.common.integration.app.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(
    basePackages = {
      "com.dxp.*",
      "org.openapitools.configuration",
      "cross.commons.*"
    })
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
