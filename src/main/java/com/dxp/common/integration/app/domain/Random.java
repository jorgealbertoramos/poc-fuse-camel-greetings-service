package com.dxp.common.integration.app.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class Random implements Serializable {
  
  private static final long serialVersionUID = 1L;
  
  @JsonProperty
  private Double random;

  public Double getRandom() {
    return random;
  }

  public void setRandom(Double random) {
    this.random = random;
  }

}
