package com.dxp.common.integration.app.domain;

import java.io.Serializable;

public class CacheConcurrent implements Serializable {

  private static final long serialVersionUID = 7169332810812850889L;

  String data1;
  String data2;
  
  /**
   * Structure for concurrent cache testing.
   */
  public CacheConcurrent(String data1, String data2) {
    super();
    this.data1 = data1;
    this.data2 = data2;
  }
  
  public String getData1() {
    return data1;
  }
  
  public void setData1(String data1) {
    this.data1 = data1;
  }
  
  public String getData2() {
    return data2;
  }
  
  public void setData2(String data2) {
    this.data2 = data2;
  }  
}