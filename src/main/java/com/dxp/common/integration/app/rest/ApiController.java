package com.dxp.common.integration.app.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1")
public class ApiController {
  
  private final Logger log = LoggerFactory.getLogger(ApiController.class);

  @RequestMapping(
      value = "/hello",
      method = RequestMethod.GET)
  public String sayHello() {
    return "test hello";
  }
  
  @RequestMapping(value = "/privateHello",
      method = RequestMethod.GET)
  public String privateHello() {
    return "private Hello";
  }

  
  /**
   * Controller for logger integration test.
   * @return correct logging string
   */
  @RequestMapping(value = "/logger",
      method = RequestMethod.GET)
  public String logger() {
    log.debug("Starting logger test");
    log.info("Info Logger test");
    log.error("Error Logger test");
    return "loggerOK";
  }
  
}